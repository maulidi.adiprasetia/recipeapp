/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { Node } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import react from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { UserCreds } from './src/constants/Constants';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NavigationContainer, ThemeProvider } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import HomeScreen from './src/views/HomeScreen';
import LoginScreen from './src/views/LoginScreen';
import CustomisableAlert from 'react-native-customisable-alert';
import RegisterScreen from './src/views/RegisterScreen';
import TermsScreen from './src/views/TermsScreen';
import RecipeDetailScreen from './src/views/RecipeDetailScreen';
import FavoriteRecipeScreen from './src/views/FavoriteRecipeScreen';

const theme = {
  colors: {
    primary: Colors.primary,
    colorPrimary: Colors.primary,
    primaryColor: Colors.primary,
  }
}

const Stack = createStackNavigator()
export const AuthContext = React.createContext()

const App: () => Node = () => {

  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token
          }
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignOut: false,
            userToken: action.token
          }
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignOut: true,
            userToken: null
          }
        case 'LOADING_COMPLETE':
          return {
            ...prevState,
            isLoading: false
          }
      }
    },
    {
      isLoading: true,
      isSignOut: false,
      userToken: null
    }
  )

  const authContextValue = React.useMemo(
    () => ({
      signIn: async data => {
        try {
          const userToken = await AsyncStorage.getItem(UserCreds.ACCESS_TOKEN_KEY)
          dispatch({ type: 'SIGN_IN', token: userToken })
        } catch (error) {
          console.log(error)
        }
      },
      signOut: async data => {
        try {
          await AsyncStorage.clear()
          dispatch({ type: 'SIGN_OUT' })
        } catch (error) {
          console.log(error)
        }
      },
      doneLoading: async data => {
        dispatch({ type: 'LOADING_COMPLETE' })
      }
    }),
    []
  )

  React.useEffect(() => {
    const getUserToken = async () => {
      let userToken = ''

      try {
        userToken = await AsyncStorage.getItem(UserCreds.ACCESS_TOKEN_KEY)
      } catch (error) {
        console.log(error)
      }

      console.log('Access Token : ', userToken)
      dispatch({ type: 'SIGN_IN', token: userToken })
    }

    getUserToken()
  }, [])

  return (
    <SafeAreaProvider>
      <StatusBar backgroundColor='#fce1e4' barStyle='dark-content' />
      <ThemeProvider
        theme={theme}>
        <CustomisableAlert
          titleStyle={{
            fontSize: 18,
            fontWeight: "bold",
            borderRadius: 18
          }}
        />
        <NavigationContainer>
          <AuthContext.Provider value={authContextValue}>
            <Stack.Navigator>
              {
                state.userToken ? (
                  <>
                    <Stack.Screen name='Home' component={HomeScreen} options={{ headerShown: false }} />
                    <Stack.Screen name='RecipeDetail' component={RecipeDetailScreen} options={{ headerShown: false }} />
                    <Stack.Screen name='FavoriteRecipe' component={FavoriteRecipeScreen} options={{ headerShown: false }} />
                  </>
                ) : (
                  <>
                    <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
                    <Stack.Screen name='Register' component={RegisterScreen} options={{ headerShown: false }} />
                    <Stack.Screen name='Terms' component={TermsScreen} options={{ headerShown: false }} />
                  </>
                )
              }
            </Stack.Navigator>
          </AuthContext.Provider>
        </NavigationContainer>
      </ThemeProvider>
    </SafeAreaProvider>
  );
};

export default App;

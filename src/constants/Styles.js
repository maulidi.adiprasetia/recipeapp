import { StyleSheet } from 'react-native'
import Colors from './Colors'

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentWrapper: {
        padding: 16
    },
    inputStyle: {
        paddingHorizontal: 12,
        paddingVertical: 8,
        borderWidth: 1,
        borderRadius: 19,
    },
    errorInputStyle: {
        borderColor: Colors.red
    },
    passInputStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 12,
        borderWidth: 1,
        borderRadius: 19,
    },
    errorMsg: {
        color: Colors.red
    },
    loadingIndicator: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black',
        opacity: 0.6,
        elevation: 10,
        zIndex: 200
    },
    recipeContainer: {
        paddingHorizontal: 16,
        paddingTop: 16,
    },
    recipeImg: {
        position: 'relative',
        height: 128,
        width: '100%',
        borderRadius: 18
    },
    recipeName: {
        position: 'absolute',
        bottom: 8,
        left: 32,
        fontSize: 18,
        fontWeight: '700',
        color: 'white'
    },
})

export default style
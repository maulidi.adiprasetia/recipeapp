export default {
    primary: '#F3717F',
    red: '#FF0000',
    greyLight: '#C4C4C4',
    facebook: '#0057EC',
}
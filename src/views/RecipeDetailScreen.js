import React, { useCallback, useEffect, useRef, useState } from "react";
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { showErrorAlert } from "../components/CustomAlert";
import style from "../constants/Styles";
import { deleteFavorite, getDetailRecipe, postFavorite } from "../services/RecipeServices";
import Icon from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from "@react-navigation/native";
import Colors from "../constants/Colors";
import CustomLoader from '../components/CustomLoader'
import Animated from "react-native-reanimated";

const RecipeDetailScreen = ({ route }) => {
    const { recipeId, favorite } = route.params
    const nav = useNavigation()
    const [data, setData] = useState({
        name: '',
        description: '',
        imageUrl: ''
    })
    const [isFavorite, setIsFavorite] = useState(favorite)
    const [isLoading, setIsLoading] = useState(false)

    const getDetail = useCallback(async () => {
        getDetailRecipe(recipeId).then(response => {
            if (response.data != null) {
                setData({
                    name: response.data.name,
                    description: response.data.description,
                    imageUrl: response.data.imageUrl
                })
            } else {
                showErrorAlert(
                    'Something Wrong Occured',
                    false
                )
            }
        }).catch(e => {
            console.log(e)
        })
    }, [])

    useEffect(() => {
        getDetail()
    }, [])

    const handleFavClicked = () => {
        setIsLoading(true)
        if (isFavorite) {
            deleteFavorite(recipeId).then(response => {
                if (response) {
                    setIsFavorite(false)
                    setIsLoading(false)
                } else {
                    showErrorAlert(
                        'Something Wrong Occured',
                        false
                    )
                }
            })
        } else {
            postFavorite(recipeId).then(response => {
                if (response) {
                    setIsFavorite(true)
                    setIsLoading(false)
                } else {
                    showErrorAlert(
                        'Something Wrong Occured',
                        false
                    )
                }
            })
        }
    }

    const scrolling = useRef(new Animated.Value(0)).current
    const translation = scrolling.interpolate({
        inputRange: [0, 100],
        outputRange: [200, 75],
        extrapolate: 'clamp'
    })
    const overlayOpacity = scrolling.interpolate({
        inputRange: [10, 100],
        outputRange: [0, 0.6],
        extrapolate: 'clamp'
    })
    const titleToolbarOpacity = scrolling.interpolate({
        inputRange: [10, 100],
        outputRange: [0, 1],
        extrapolate: 'clamp'
    })
    const titleOpacity = scrolling.interpolate({
        inputRange: [0, 20],
        outputRange: [1, 0],
        extrapolate: 'clamp'
    })

    const detailStyle = StyleSheet.create({
        image: {
            height: translation,
        },
        imageOverlay: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            height: translation,
            opacity: overlayOpacity,
            backgroundColor: 'black',
        },
        name: {
            marginBottom: 8,
            fontSize: 18,
            fontWeight: '700',
            color: 'black',
            opacity: titleOpacity
        },
        nameToolbar: {
            position: 'absolute',
            top: 32,
            left: 64,
            fontSize: 18,
            fontWeight: '700',
            opacity: titleToolbarOpacity,
            color: 'white',
            zIndex: 100
        },
        backContainer: {
            position: 'absolute',
            top: 32,
            left: 16,
            width: 30,
            height: 30,
            padding: 4,
            borderRadius: 15,
            backgroundColor: 'white',
            zIndex: 100
        },
        favContainer: {
            position: 'absolute',
            top: 32,
            right: 16,
            width: 30,
            height: 30,
            padding: 4,
            borderRadius: 15,
            backgroundColor: 'white',
            zIndex: 100
        }
    })

    return (
        <>
            <Animated.ScrollView style={{ flex: 1 }} stickyHeaderIndices={[0]} showsVerticalScrollIndicator={false} onScroll={Animated.event(
                [{
                    nativeEvent: {
                        contentOffset: {
                            y: scrolling
                        }
                    }
                }],
                { useNativeDriver: true }
            )}>
                <View style={detailStyle.header}>
                    <TouchableOpacity
                        style={detailStyle.backContainer}
                        onPress={() => nav.goBack()}
                    >
                        <Icon style={{ alignSelf: 'center' }} name="arrow-left" size={20} color='black' />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={detailStyle.favContainer}
                        onPress={() => handleFavClicked()}
                    >
                        <Icon
                            style={{ alignSelf: 'center', justifyContent: 'center' }}
                            name={isFavorite ? 'heart' : 'heart-o'}
                            size={20}
                            color={isFavorite ? Colors.red : 'black'}
                        />
                    </TouchableOpacity>
                    <Animated.Text style={detailStyle.nameToolbar}>{data.name}</Animated.Text>
                    <Animated.Image style={detailStyle.image} source={{ uri: data.imageUrl }} />
                    <Animated.View style={detailStyle.imageOverlay}></Animated.View>
                </View>
                <View style={style.container}>
                    <View style={style.contentWrapper}>
                        <Animated.Text style={detailStyle.name}>{data.name}</Animated.Text>
                        <Text style={{ color: 'black' }}>{data.description}</Text>
                    </View>
                </View>
            </Animated.ScrollView>
            {
                isLoading &&
                <CustomLoader />
            }
        </>
    )
}

export default RecipeDetailScreen
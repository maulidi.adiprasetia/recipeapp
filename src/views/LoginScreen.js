import React, { useContext, useState } from "react";
import { View, Text, StyleSheet, Image, Keyboard } from 'react-native'
import { ScrollView, TextInput, TouchableOpacity } from "react-native-gesture-handler";
import Colors from '../constants/Colors'
import PasswordInput from "../components/PasswordInput";
import style from "../constants/Styles";
import CustomButton from "../components/CustomButton";
import CustomLoader from '../components/CustomLoader'
import * as yup from 'yup'
import { Formik } from "formik";
import { Login } from "../services/AuthServices";
import { showErrorAlert, showLoginMethod } from "../components/CustomAlert";
import { AuthContext } from "../../App";
import { GoogleSignin } from "react-native-login-google";
import { useNavigation } from "@react-navigation/native";

const loginStyle = StyleSheet.create({
    contentWrapper: {
        paddingTop: 30,
        paddingHorizontal: 16,
        paddingBottom: 24,
        justifyContent: 'space-between'
    },
    logo: {
        width: 277,
        height: 189,
        marginBottom: 32,
        alignSelf: 'center'
    },
    inputContainer: {
        width: '100%',
        alignSelf: 'flex-start',
    },
    buttonContainer: {
        width: '100%',
        marginTop: 12
    }
})

const initialValue = {
    email: '',
    password: '',
}

const validationSchema = yup.object().shape({
    email: yup.string()
        .email('Wrong Email Format')
        .max(20, 'Maximum 20 Characters')
        .required('Field Cannot Empty'),
    password: yup.string()
        .min(6, 'Minimum 6 Characters')
        .required('Field Cannot Empty')
})

const handleGoogle = () => {
    console.log('Login with Google')
    // GoogleSignin.configure({
    //     androidClientId: '662809055623-qgksbvq4vfo0i4cuf7d1q7ttqum90fem.apps.googleusercontent.com',
    //     iosClientId: '662809055623-9urp5bvlpr19p15ejaasuiorvrth5d1o.apps.googleusercontent.com'
    // })

    // GoogleSignin.hasPlayServices().then((hasPlayServices) => {
    //     if (hasPlayServices) {
    //         GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true }); // <-- Add this
    //         GoogleSignin.signIn().then((userInfo) => {
    //             console.log(JSON.stringify(userInfo))
    //         }).catch(e => {
    //             console.log('Error : ', e)
    //         })
    //     }
    // }).catch(e => {
    //     console.log('Errors : ', e)
    // })
}

const handleFacebook = () => {
    console.log('Login with Facebook')
}

const LoginScreen = () => {
    const nav = useNavigation()
    const [isSubmitTapped, setIsSubmitTapped] = useState(false)
    const [isPassVisible, setIsPassVisible] = useState(false)
    const { signIn } = useContext(AuthContext)

    const handleLogin = (values, actions) => {
        Login(values).then(response => {
            signIn()
        }).catch(err => {
            actions.setSubmitting(false)
            showErrorAlert('Something Went Wrong', false, () => { })
        })
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>
            <Formik initialValues={initialValue} validationSchema={validationSchema} onSubmit={(values, actions) => handleLogin(values, actions)}>
                {({ values, errors, isValid, isSubmitting, handleChange, handleSubmit }) => (
                    <View style={[style.container, loginStyle.contentWrapper]}>
                        <View>
                            <Image style={loginStyle.logo} source={require('../assets/images/logo.png')} />
                            <View style={loginStyle.inputContainer}>
                                <Text style={{ marginLeft: 11 }}>Email</Text>
                                <TextInput
                                    style={[style.inputStyle, errors.email && isSubmitTapped && style.errorInputStyle]}
                                    keyboardType='email-address'
                                    textContentType='emailAddress'
                                    onChangeText={handleChange('email')}
                                    placeholder="Email"
                                    value={values.email}
                                />
                                {
                                    errors.email && isSubmitTapped &&
                                    <Text style={[style.errorMsg, { marginLeft: 11 }]}>{errors.email}</Text>
                                }
                                <Text style={[{ marginLeft: 11 }, { marginTop: 20 }]}>Password</Text>
                                <PasswordInput
                                    onChangeText={handleChange('password')}
                                    visible={isPassVisible}
                                    onEyeTapped={() => setIsPassVisible(!isPassVisible)}
                                    style={errors.password && isSubmitTapped && style.errorInputStyle}
                                />
                                {
                                    errors.password && isSubmitTapped &&
                                    <Text style={[style.errorMsg, { marginLeft: 11 }]}>{errors.password}</Text>
                                }
                            </View>
                        </View>
                        <View style={loginStyle.buttonContainer}>
                            <CustomButton
                                title='LOGIN'
                                types='primary'
                                onPress={() => {
                                    setIsSubmitTapped(true)
                                    Keyboard.dismiss()
                                    handleSubmit()
                                }}
                                disabled={!isValid && isSubmitTapped}
                            />
                            <View style={{ flexDirection: 'row', marginTop: 8, marginBottom: 32, justifyContent: 'center' }}>
                                <Text>Don't have an account?</Text>
                                <TouchableOpacity
                                    onPress={() => nav.navigate('Register')}
                                >
                                    <Text style={{ color: Colors.primary }}> Register Now</Text>
                                </TouchableOpacity>
                            </View>
                            <CustomButton
                                title='Sign in with another method'
                                types='other'
                                onPress={() => showLoginMethod(
                                    () => handleGoogle(),
                                    () => handleFacebook()
                                )}
                            />
                        </View>
                        {
                            isSubmitting &&
                            <CustomLoader />
                        }
                    </View>
                )}
            </Formik>
        </ScrollView>
    )
}

export default LoginScreen
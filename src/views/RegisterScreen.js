import { useNavigation } from "@react-navigation/native";
import React, { useContext, useState } from "react";
import { Keyboard, StyleSheet, Text, View } from "react-native";
import CustomToolbar from "../components/CustomToolbar";
import style from "../constants/Styles";
import * as yup from 'yup'
import { Formik } from "formik";
import CustomButton from "../components/CustomButton";
import { ScrollView, TextInput, TouchableOpacity } from "react-native-gesture-handler";
import PasswordInput from "../components/PasswordInput";
import { CheckBox } from "react-native-elements";
import Colors from "../constants/Colors";
import CustomLoader from "../components/CustomLoader";
import { Register } from "../services/AuthServices";
import { AuthContext } from "../../App";

const initialValue = {
    name: '',
    phone: '',
    email: '',
    password: '',
    confPassword: ''
}

const phoneRegex = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const validationSchema = yup.object().shape({
    name: yup.string()
        .required('Field Cannot Empty'),
    phone: yup.string()
        .length(13, 'Only Accept 13 Characters')
        .matches(phoneRegex, 'Wrong Phone Format')
        .required('Field Cannot Empty'),
    email: yup.string()
        .email('Wrong Email Format')
        .max(20, 'Maximum 20 Characters')
        .required('Field Cannot Empty'),
    password: yup.string()
        .min(6, 'Minimum 6 Characters')
        .required('Field Cannot Empty'),
    confPassword: yup.string()
        .min(6, 'Minimum 6 Characters')
        .oneOf([yup.ref('password')], 'Password Not Match')
        .required('Field Cannot Empty')
})

const RegisterScreen = () => {
    const { signIn } = useContext(AuthContext)
    const nav = useNavigation()
    const [isPassVisible, setIsPassVisible] = useState(false)
    const [isConfPassVisible, setIsConfPassVisible] = useState(false)
    const [isSubmitTapped, setIsSubmitTapped] = useState(false)
    const [isChecked, setIsChecked] = useState(false)
    const [isSubmit, setIsSubmit] = useState(false)

    const handleSubmit = (values) => {
        setIsSubmit(true)
        Register(values).then(response => {
            signIn()
        }).catch(e => {
            setIsSubmit(false)
            showErrorAlert('Something Went Wrong', false, () => { })
        })
    }

    return (
        <View style={style.container}>
            <CustomToolbar
                title='REGISTER'
                hasBack={true}
                onBackPressed={() => nav.goBack()}
                hasMenu={false}
            />
            <ScrollView contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>
                <View style={[style.contentWrapper, { flex: 1 }]}>
                    <Formik initialValues={initialValue} validationSchema={validationSchema} onSubmit={(values) => handleSubmit(values)} >
                        {({ values, isValid, errors, handleSubmit, handleChange }) => (
                            <View style={{ justifyContent: 'space-between', flex: 1 }}>
                                <View>
                                    <Text style={{ marginLeft: 11 }}>Name</Text>
                                    <TextInput
                                        style={[style.inputStyle, errors.name && isSubmitTapped && style.errorInputStyle]}
                                        textContentType='name'
                                        onChangeText={handleChange('name')}
                                        placeholder="Input your full name here"
                                        value={values.name}
                                    />
                                    {
                                        errors.name && isSubmitTapped &&
                                        <Text style={[style.errorMsg, { marginLeft: 11 }]}>{errors.name}</Text>
                                    }
                                    <Text style={{ marginLeft: 11, marginTop: 20 }}>Phone</Text>
                                    <TextInput
                                        style={[style.inputStyle, errors.phone && isSubmitTapped && style.errorInputStyle]}
                                        keyboardType='phone-pad'
                                        textContentType='telephoneNumber'
                                        onChangeText={handleChange('phone')}
                                        placeholder="Input phone number"
                                        value={values.phone}
                                    />
                                    {
                                        errors.phone && isSubmitTapped &&
                                        <Text style={[style.errorMsg, { marginLeft: 11 }]}>{errors.phone}</Text>
                                    }
                                    <Text style={{ marginLeft: 11, marginTop: 20 }}>Email</Text>
                                    <TextInput
                                        style={[style.inputStyle, errors.email && isSubmitTapped && style.errorInputStyle]}
                                        keyboardType='email-address'
                                        textContentType='emailAddress'
                                        onChangeText={handleChange('email')}
                                        placeholder="Email"
                                        value={values.email}
                                    />
                                    {
                                        errors.email && isSubmitTapped &&
                                        <Text style={[style.errorMsg, { marginLeft: 11 }]}>{errors.email}</Text>
                                    }
                                    <Text style={{ marginLeft: 11, marginTop: 20 }}>Password</Text>
                                    <PasswordInput
                                        onChangeText={handleChange('password')}
                                        visible={isPassVisible}
                                        onEyeTapped={() => setIsPassVisible(!isPassVisible)}
                                        style={errors.password && isSubmitTapped && style.errorInputStyle}
                                    />
                                    {
                                        errors.password && isSubmitTapped &&
                                        <Text style={[style.errorMsg, { marginLeft: 11 }]}>{errors.password}</Text>
                                    }
                                    <Text style={{ marginLeft: 11, marginTop: 20 }}>Re-Password</Text>
                                    <PasswordInput
                                        onChangeText={handleChange('confPassword')}
                                        visible={isConfPassVisible}
                                        onEyeTapped={() => setIsConfPassVisible(!isConfPassVisible)}
                                        style={errors.confPassword && isSubmitTapped && style.errorInputStyle}
                                    />
                                    {
                                        errors.confPassword && isSubmitTapped &&
                                        <Text style={[style.errorMsg, { marginLeft: 11 }]}>{errors.confPassword}</Text>
                                    }
                                    <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
                                        <CheckBox
                                            containerStyle={{ marginLeft: -10, marginRight: -4 }}
                                            checkedColor={Colors.primary}
                                            checked={isChecked}
                                            onIconPress={() => setIsChecked(!isChecked)}
                                        />
                                        <Text>I have read the </Text>
                                        <TouchableOpacity
                                            onPress={() => nav.navigate('Terms')}
                                        >
                                            <Text style={{ color: Colors.primary }}>Terms of Service</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View>
                                    <CustomButton
                                        title='Register'
                                        types='primary'
                                        disabled={!isValid || !isChecked && isSubmitTapped}
                                        onPress={() => {
                                            setIsSubmitTapped(true)
                                            Keyboard.dismiss()
                                            handleSubmit()
                                        }}
                                    />
                                </View>
                            </View>
                        )}
                    </Formik>
                </View>
            </ScrollView>
            {
                isSubmit &&
                <CustomLoader />
            }
        </View>
    )
}

export default RegisterScreen
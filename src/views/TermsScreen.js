import { useNavigation } from "@react-navigation/native";
import React from "react";
import { View } from "react-native";
import WebView from "react-native-webview";
import CustomToolbar from "../components/CustomToolbar";
import style from "../constants/Styles";

const TermsScreen = () => {
    const nav = useNavigation()

    return (
        <View style={style.container}>
            <CustomToolbar
                title='TERMS OF SERVICE'
                hasBack={false}
                hasMenu={true}
                menuIcon='close'
                onMenuPressed={() => nav.goBack()}
            />
            <WebView source={{ uri: 'https://reactnative.dev/' }} />
        </View>
    )
}

export default TermsScreen
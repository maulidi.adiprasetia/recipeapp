import { useNavigation } from "@react-navigation/native";
import React, { useCallback, useEffect, useState } from "react";
import { FlatList, View } from "react-native";
import { showErrorAlert } from "../components/CustomAlert";
import CustomToolbar from "../components/CustomToolbar";
import { emptyBox } from "../components/EmptyBox";
import recipeRenderItem from "../components/RecipeCard";
import { recipeFooterShimmer, recipeShimmer } from "../components/Shimmer";
import style from "../constants/Styles";
import { getFavoriteRecipes, getMoreFavoriteRecipes } from "../services/RecipeServices";

const FavoriteRecipeScreen = () => {
    const nav = useNavigation()
    const [isFetching, setIsFetching] = useState(false)
    const [isEndData, setIsEndData] = useState(false)
    const [isFetchMore, setIsFetchMore] = useState(false)
    const [data, setData] = useState([])
    let currentPage = 1

    const getFavRecipe = useCallback(async () => {
        await getFavoriteRecipes().then(response => {
            if (response.data.length > 0) {
                currentPage = 1
                setData(response.data)
            }
        }).catch(e => {
            console.log(e)
        })
    }, [])

    const getMoreFavRecipe = useCallback(async () => {
        await getMoreFavoriteRecipes(currentPage + 1).then(response => {
            if (response.data.length > 0) {
                currentPage = response.meta.current_page
                setData(prevData => [
                    ...prevData, ...response.data
                ])
            } else {
                setIsEndData(true)
                showErrorAlert(
                    'Last Item Reached',
                    false
                )
            }
        }).catch(e => {
            console.log(e)
        })
    })

    useEffect(() => {
        setIsFetching(true)
        getFavRecipe().then(_ => {
            setIsFetching(false)
        })
    }, [])

    return (
        <View style={style.container}>
            <CustomToolbar
                title='FAVORITE RECIPES'
                hasBack={true}
                onBackPressed={() => nav.goBack()}
                hasMenu={false}
            />
            {
                isFetching ?
                    recipeShimmer() :
                    <FlatList
                        data={data}
                        renderItem={({ item, index }) => recipeRenderItem(item, index, nav, true)}
                        keyExtractor={(item, index) => index.toString()}
                        refreshing={isFetching}
                        onRefresh={() => {
                            setIsFetching(true)
                            setIsEndData(false)
                            getFavRecipe().then(_ => {
                                setIsFetching(false)
                            })
                        }}
                        onEndReached={() => {
                            if (!isFetchMore && !isEndData) {
                                setIsFetchMore(true)
                                getMoreFavRecipe().then(_ => {
                                    setIsFetchMore(false)
                                })
                            }
                        }}
                        onEndReachedThreshold={0.5}
                        ListFooterComponent={() => (
                            isFetchMore &&
                            recipeFooterShimmer()
                        )}
                        ListEmptyComponent={!isFetching && emptyBox()}
                        style={{ marginBottom: 16 }}
                    />
            }
        </View>
    )
}

export default FavoriteRecipeScreen
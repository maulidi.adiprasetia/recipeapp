import { useNavigation } from "@react-navigation/native";
import React, { useCallback, useEffect, useState, useRef, useMemo, useContext } from "react";
import { FlatList, Image, SafeAreaView, StyleSheet, Text, View } from "react-native";
import { showErrorAlert, showRecipePreview } from "../components/CustomAlert";
import CustomToolbar from "../components/CustomToolbar";
import { recipeFooterShimmer, recipeShimmer } from "../components/Shimmer";
import style from "../constants/Styles";
import { getMoreRecipes, getRecipes } from "../services/RecipeServices";
import BottomSheet from '@gorhom/bottom-sheet'
import Colors from "../constants/Colors";
import CustomButton from "../components/CustomButton";
import recipeRenderItem from "../components/RecipeCard";
import { AuthContext } from "../../App";
import { closeAlert } from "react-native-customisable-alert";
import { emptyBox } from "../components/EmptyBox";
import CustomLoader from "../components/CustomLoader";

const recipeStyle = StyleSheet.create({
    bottomSheetHandle: {
        borderWidth: 2,
        width: 50,
        borderColor: Colors.greyLight,
        borderRadius: 2,
        alignSelf: 'center'
    },
    bottomSheetContainer: {
        paddingTop: 12,
        paddingHorizontal: 16,
        paddingBottom: 28
    },
    bottomSheetTitle: {
        paddingBottom: 10,
        marginBottom: 18,
        fontSize: 16,
        fontWeight: '700',
        textAlign: 'center',
        color: 'black',
        borderStyle: 'dashed',
        borderBottomWidth: 1,
    }
})

const HomeScreen = () => {
    const nav = useNavigation()
    const { signOut } = useContext(AuthContext)
    const [isFetching, setIsFetching] = useState(false)
    const [isFetchMore, setIsFetchMore] = useState(false)
    const [isEndData, setIsEndData] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
    const [recipes, setRecipes] = useState([])
    let currentPage = 2

    const getRecipe = useCallback(async () => {
        await getRecipes().then(response => {
            if (response != null) {
                currentPage = 2
                setRecipes(response)
            }
        }).catch(e => {
            console.log('Error : ', e)
        })
    }, [])

    const getMoreRecipe = useCallback(async () => {
        await getMoreRecipes(currentPage + 1).then(response => {
            if (response.data.length > 0) {
                currentPage = response.meta.current_page
                setRecipes(prevRecipes => [
                    ...prevRecipes, ...response.data
                ])
            } else {
                setIsEndData(true)
                showErrorAlert(
                    'Last Item Reached',
                    false
                )
            }
        }).catch(e => {
            console.log(e)
        })
    }, [])

    useEffect(() => {
        setIsFetching(true)
        getRecipe().then(_ => {
            setIsFetching(false)
        })
    }, [])

    const bottomSheetRef = useRef(null)
    const snapPoints = useMemo(() => ['4%', '40%'], [])
    const handleSheetChanges = useCallback((index) => {
        console.log('handle sheet : '), index
    })

    const bottomSheetHandle = () => {
        return (
            <>
                <View style={[recipeStyle.bottomSheetHandle, { marginTop: 10, marginBottom: 2 }]}></View>
                <View style={recipeStyle.bottomSheetHandle}></View>
            </>
        )
    }

    return (
        <View style={style.container}>
            <CustomToolbar
                title='RECIPES'
                hasBack={false}
                hasMenu={false}
            />
            <SafeAreaView>
                {
                    isFetching ?
                        recipeShimmer() :
                        <FlatList
                            data={recipes}
                            renderItem={({ item, index }) => recipeRenderItem(item, index, nav, false)}
                            keyExtractor={(data, index) => index.toString()}
                            refreshing={isFetching}
                            onRefresh={() => {
                                setIsFetching(true)
                                setIsEndData(false)
                                getRecipe().then(_ => {
                                    setIsFetching(false)
                                })
                            }}
                            ListEmptyComponent={!isFetching && emptyBox()}
                            onEndReached={() => {
                                if (!isFetchMore && !isEndData) {
                                    setIsFetchMore(true)
                                    getMoreRecipe().then(_ => {
                                        setIsFetchMore(false)
                                    })
                                }
                            }}
                            onEndReachedThreshold={0.5}
                            ListFooterComponent={() => (
                                isFetchMore &&
                                recipeFooterShimmer()
                            )}
                            style={{ marginBottom: 90 }}
                        />
                }
            </SafeAreaView>
            <BottomSheet
                ref={bottomSheetRef}
                snapPoints={snapPoints}
                index={0}
                onChange={handleSheetChanges}
                detached={true}
                style={{ borderTopLeftRadius: 14, borderTopRightRadius: 14, borderWidth: 1 }}
                handleComponent={bottomSheetHandle}
            >
                <View style={recipeStyle.bottomSheetContainer}>
                    <Text style={recipeStyle.bottomSheetTitle}>MENU</Text>
                    <CustomButton
                        title='PROFILE'
                        types='secondary'
                        btnStyle={{ marginBottom: 12 }}
                        onPress={() => { }}
                    />
                    <CustomButton
                        title='FAVORITE'
                        types='secondary'
                        btnStyle={{ marginBottom: 12 }}
                        onPress={() => nav.navigate('FavoriteRecipe')}
                    />
                    <CustomButton
                        title='LOG OUT'
                        types='danger'
                        onPress={() => showErrorAlert(
                            'Do you want to exit?',
                            true,
                            () => {
                                closeAlert()
                                setIsLoading(true)
                                signOut()
                            }
                        )}
                    />
                </View>
            </BottomSheet>
            {
                isLoading &&
                <CustomLoader />
            }
        </View>
    )
}

export default HomeScreen
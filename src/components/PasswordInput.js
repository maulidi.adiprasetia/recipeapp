import React from 'react'
import { View } from 'react-native'
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";
import Icon from 'react-native-vector-icons/FontAwesome';
import style from '../constants/Styles'

const PasswordInput = (props) => {
    return (
        <View style={[style.passInputStyle, props.style]}>
            <TextInput
                {...props}
                style={[{ flex: 1 }]}
                textContentType='password'
                secureTextEntry={!props.visible}
                placeholder="Password"
            />
            <TouchableOpacity
                onPress={props.onEyeTapped}
            >
                <Icon name={props.visible ? 'eye-slash' : 'eye'} size={16} color='black' />
            </TouchableOpacity>
        </View>
    )
}

export default PasswordInput
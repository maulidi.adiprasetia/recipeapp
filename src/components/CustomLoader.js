import React from "react"
import { ActivityIndicator, View } from "react-native"
import Colors from "../constants/Colors"
import style from "../constants/Styles"


const CustomLoader = () => {
    return (
        <View style={style.loadingIndicator}>
            <ActivityIndicator size={70} color={Colors.primary} />
        </View>
    )
}

export default CustomLoader
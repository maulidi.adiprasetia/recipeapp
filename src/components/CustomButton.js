import React from "react";
import { StyleSheet } from "react-native";
import { Button } from "react-native-elements";
import Colors from "../constants/Colors";

const customBtnStyle = StyleSheet.create({
    title: {
        fontSize: 16,
        fontWeight: '500',
    },
    titlePrimary: {
        color: 'white'
    },
    titleSecondary: {
        color: 'black'
    },
    btn: {
        paddingVertical: 8,
        borderRadius: 19,
    },
    btnPrimary: {
        backgroundColor: Colors.primary
    },
    btnSecondary: {
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: 'black',
    },
    btnDanger: {
        backgroundColor: Colors.red
    },
    btnFacebook: {
        backgroundColor: Colors.facebook
    },
    btnOther: {
        backgroundColor: 'black'
    }
})

const CustomButton = (props) => {
    return (
        <Button
            title={props.title}
            titleStyle={[
                customBtnStyle.title,
                props.types == 'secondary' ? customBtnStyle.titleSecondary : customBtnStyle.titlePrimary
            ]}
            buttonStyle={[
                customBtnStyle.btn,
                props.types == 'primary' ?
                    customBtnStyle.btnPrimary :
                    props.types == 'secondary' ?
                        customBtnStyle.btnSecondary :
                        props.types == 'danger' ?
                            customBtnStyle.btnDanger :
                            customBtnStyle.btnOther
                , props.btnStyle]}
            disabledStyle={{ backgroundColor: Colors.greyLight }}
            disabledTitleStyle={{ color: 'white' }}
            disabled={props.disabled}
            onPress={props.onPress}
        />
    )
}

export default CustomButton
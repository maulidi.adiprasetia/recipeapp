import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from 'react-native-vector-icons/FontAwesome';

const toolbarStyle = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingHorizontal: 20,
        paddingVertical: 16,
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    }
})

const CustomToolbar = (props) => {
    return (
        <View style={toolbarStyle.container}>
            {
                props.hasBack ?
                    <View style={{ flex: 1 }}>
                        <TouchableOpacity
                            onPress={props.onBackPressed}
                        >
                            <Icon name="arrow-left" size={20} color='black' />
                        </TouchableOpacity>
                    </View> :
                    <View style={{ flex: 1 }} />
            }
            <View style={{ flex: 4 }}>
                <Text style={toolbarStyle.title}>{props.title}</Text>
            </View>
            {
                props.hasMenu ?
                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <TouchableOpacity
                            onPress={props.onMenuPressed}
                        >
                            <Icon name={props.menuIcon} size={20} color='black' />
                        </TouchableOpacity>
                    </View> :
                    <View style={{ flex: 1 }} />
            }
        </View>
    )
}

export default CustomToolbar
import React from "react"
import AnimatedLottieView from "lottie-react-native"
import { Image, StyleSheet, Text, TouchableHighlight, View } from "react-native"
import { closeAlert, showAlert } from "react-native-customisable-alert"
import Icon from 'react-native-vector-icons/FontAwesome';
import { Button } from "react-native-elements"
import Colors from "../constants/Colors"
import CustomButton from "./CustomButton";
import { TouchableOpacity } from "react-native-gesture-handler";

const alertStyle = StyleSheet.create({
    container: {
        width: '80%',
        flexDirection: 'column',
        borderRadius: 18,
        backgroundColor: 'white',
    },
    lottie: {
        width: 150,
        height: 150,
        marginBottom: 16,
        alignSelf: 'center',
    },
    title: {
        marginBottom: 42,
        fontSize: 16,
        fontWeight: '700',
        alignSelf: 'center',
        textAlign: 'center'
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    btn: {
        height: 44,
        paddingVertical: 12,
        borderRadius: 18,
        backgroundColor: 'white'
    },
    btnTitle: {
        color: 'black',
        fontSize: 16,
        fontWeight: '700',
    },
    loginContainer: {
        paddingTop: 14,
        paddingHorizontal: 8,
        paddingBottom: 20,
        alignItems: 'center'
    },
    loginTitle: {
        paddingHorizontal: 8,
        textAlign: 'center'
    },
    btnMethod: {
        flexDirection: 'row',
        paddingVertical: 8,
        paddingHorizontal: 16,
        marginBottom: 8,
        minWidth: 250,
        borderRadius: 19,
        justifyContent: 'center',
    },
    recipeImg: {
        height: 176,
        marginBottom: 16,
        borderTopLeftRadius: 18,
        borderTopRightRadius: 18
    },
    recipeDesc: {
        marginBottom: 16,
        textAlign: 'center',
    },
    closePreview: {
        position: 'absolute',
        top: 8,
        right: 8,
        width: 30,
        height: 30,
        padding: 4,
        borderRadius: 15,
        backgroundColor: 'white'
    }
})

export const showErrorAlert = (title, isDouble, onPositivePress) => {
    showAlert({
        alertType: 'custom',
        customAlert: (
            <View style={alertStyle.container}>
                <AnimatedLottieView style={alertStyle.lottie} source={require('../assets/lottie/alert.json')} loop autoPlay />
                <Text style={alertStyle.title}>{title}</Text>
                <View style={alertStyle.btnContainer}>
                    {
                        isDouble ?
                            <>
                                <View style={{ flex: 1 }}>
                                    <Button
                                        title='Yes'
                                        titleStyle={alertStyle.btnTitle}
                                        buttonStyle={alertStyle.btn}
                                        onPress={() => onPositivePress()}
                                    />
                                </View>
                                <View style={{ flex: 1 }}>
                                    <Button
                                        title='No'
                                        titleStyle={alertStyle.btnTitle}
                                        buttonStyle={alertStyle.btn}
                                        onPress={() => closeAlert()}
                                    />
                                </View>
                            </> :
                            <View style={{ flex: 1 }}>
                                <Button
                                    title='OK'
                                    titleStyle={alertStyle.btnTitle}
                                    buttonStyle={alertStyle.btn}
                                    onPress={() => closeAlert()}
                                />
                            </View>
                    }
                </View>
            </View>
        ),
        dismissable: true
    })
}

export const showLoginMethod = (googleClicked, fbClicked) => {
    showAlert({
        alertType: 'custom',
        customAlert: (
            <View style={[alertStyle.container, alertStyle.loginContainer]}>
                <View style={{ flexDirection: 'row', marginBottom: 14, alignItems: 'center' }}>
                    <View style={{ flex: 1, height: 1, backgroundColor: 'black' }} />
                    <View>
                        <Text style={alertStyle.loginTitle}>Sign In Method</Text>
                    </View>
                    <View style={{ flex: 1, height: 1, backgroundColor: 'black' }} />
                </View>
                <TouchableHighlight
                    underlayColor={Colors.greyLight}
                    style={[{ minWidth: 250 }, { borderRadius: 19 }]}
                    onPress={() => googleClicked()}
                >
                    <View style={[alertStyle.btnMethod, { borderWidth: 1 }]}>
                        <Icon name='google' size={20} />
                        <Text style={[{ fontSize: 16 }, { fontWeight: '700' }]}> Sign in With Google</Text>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight
                    underlayColor={Colors.greyLight}
                    style={[{ minWidth: 250 }, { borderRadius: 19 }]}
                    onPress={() => fbClicked()}
                >
                    <View style={[alertStyle.btnMethod, { backgroundColor: Colors.facebook }]}>
                        <Icon name='facebook' color='white' size={20} />
                        <Text style={[{ color: 'white' }, { fontSize: 16 }, { fontWeight: '700' }]}> Continue With Facebook</Text>
                    </View>
                </TouchableHighlight>
            </View>
        ),
        dismissable: true
    })
}

export const showRecipePreview = (imageUrl, desc, onPress) => {
    showAlert({
        alertType: 'custom',
        customAlert: (
            <View style={alertStyle.container}>
                <Image style={alertStyle.recipeImg} source={{ uri: imageUrl }} />
                <TouchableHighlight
                    style={alertStyle.closePreview}
                    underlayColor={Colors.greyLight}
                    onPress={() => closeAlert()}
                >
                    <Icon style={{ alignSelf: 'center' }} name="close" size={20} color='black' />
                </TouchableHighlight>
                <View style={{ padding: 16 }}>
                    <Text numberOfLines={5} ellipsizeMode='tail' style={alertStyle.recipeDesc}>
                        {desc}
                    </Text>
                    <CustomButton
                        title='Detail'
                        types='primary'
                        onPress={() => onPress()}
                    />
                </View>
            </View>
        ),
        dismissable: true
    })
}
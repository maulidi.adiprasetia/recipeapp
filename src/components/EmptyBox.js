import AnimatedLottieView from "lottie-react-native";
import React from "react";
import { View } from "react-native";

export const emptyBox = () => {
    return (
        <AnimatedLottieView style={{ height: 500, alignSelf: 'center', justifyContent: 'center' }} source={require('../assets/lottie/empty_box.json')} loop autoPlay />
    )
}
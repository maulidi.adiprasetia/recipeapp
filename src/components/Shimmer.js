import React from "react";
import { StyleSheet, View } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { createShimmerPlaceholder } from "react-native-shimmer-placeholder";

const shimmerStyle = StyleSheet.create({
    recipe: {
        height: 128,
        width: '100%',
        borderRadius: 18
    }
})

const ShimmerPlaceholder = createShimmerPlaceholder(LinearGradient)

export const recipeShimmer = () => {
    return (
        <>
            <View style={{ paddingHorizontal: 16, paddingTop: 16 }}>
                <ShimmerPlaceholder style={shimmerStyle.recipe} />
            </View>
            <View style={{ paddingHorizontal: 16, paddingTop: 16 }}>
                <ShimmerPlaceholder style={shimmerStyle.recipe} />
            </View>
            <View style={{ paddingHorizontal: 16, paddingTop: 16 }}>
                <ShimmerPlaceholder style={shimmerStyle.recipe} />
            </View>
            <View style={{ paddingHorizontal: 16, paddingTop: 16 }}>
                <ShimmerPlaceholder style={shimmerStyle.recipe} />
            </View>
            <View style={{ paddingHorizontal: 16, paddingTop: 16 }}>
                <ShimmerPlaceholder style={shimmerStyle.recipe} />
            </View>
            <View style={{ paddingHorizontal: 16, paddingTop: 16 }}>
                <ShimmerPlaceholder style={shimmerStyle.recipe} />
            </View>
            <View style={{ paddingHorizontal: 16, paddingTop: 16 }}>
                <ShimmerPlaceholder style={shimmerStyle.recipe} />
            </View>
            <View style={{ paddingHorizontal: 16, paddingTop: 16 }}>
                <ShimmerPlaceholder style={shimmerStyle.recipe} />
            </View>
        </>
    )
}

export const recipeFooterShimmer = () => {
    return (
        <View style={{ paddingHorizontal: 16, paddingTop: 16 }}>
            <ShimmerPlaceholder style={shimmerStyle.recipe} />
        </View>
    )
}
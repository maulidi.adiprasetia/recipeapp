import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { closeAlert } from "react-native-customisable-alert";
import style from "../constants/Styles";
import { showRecipePreview } from "./CustomAlert";

const recipeRenderItem = (itemData, index, nav, favorite) => {
    return (
        <TouchableOpacity
            onPress={() => showRecipePreview(
                itemData.imageUrl,
                itemData.description,
                () => {
                    closeAlert()
                    nav.navigate('RecipeDetail', { recipeId: itemData.id, favorite: favorite })
                }
            )}
        >
            <View style={style.recipeContainer}>
                <Image style={style.recipeImg} source={{ uri: itemData.imageUrl }} />
                <Text style={style.recipeName} >{itemData.name.toUpperCase()}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default recipeRenderItem
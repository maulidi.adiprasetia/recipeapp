import AsyncStorage from "@react-native-community/async-storage";
import { UserCreds } from "../constants/Constants";
import clientConfig from "./getClient";

const storeAccessToken = async (accessToken) => {
    try {
        await AsyncStorage.setItem(UserCreds.ACCESS_TOKEN_KEY, accessToken)
        console.log('Stored Token : ', accessToken)
    } catch (error) {
        console.log(error)
    }
}

export const Login = async (data) => {
    const response = await clientConfig.post(
        '/login', {
        email: data.email,
        password: data.password
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    if (response.data != null) {
        storeAccessToken(response.data.data.access_token)
    }

    return response.data
}

export const Register = async (data) => {
    const response = await clientConfig.post(
        '/register', {
        name: data.name,
        email: data.email,
        phone: data.phone,
        password: data.password,
        password_confirmation: data.confPassword
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    if (response.data != null) {
        storeAccessToken(response.data.data.access_token)
    }

    return response.data
}
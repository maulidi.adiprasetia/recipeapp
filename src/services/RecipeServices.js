import AsyncStorage from "@react-native-community/async-storage"
import clientConfig from "./getClient"
import { UserCreds } from "../constants/Constants";

export const getRecipes = async () => {
    const response = await clientConfig.get('/recipe')
    console.log(JSON.stringify(response.data, null, 2))

    const recipes = response.data.data

    if (response.data.meta.current_page != null) {
        await getMoreRecipes(response.data.meta.current_page + 1).then(response => {
            response.data.forEach(item => {
                recipes.push(item)
            })
        })
    }

    return recipes
}

export const getMoreRecipes = async (page) => {
    const response = await clientConfig.get(
        '/recipe', {
        params: {
            page: page
        }
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const getFavoriteRecipes = async () => {
    const accessToken = await AsyncStorage.getItem(UserCreds.ACCESS_TOKEN_KEY)
    const response = await clientConfig.get(
        '/favorite', {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const getMoreFavoriteRecipes = async (page) => {
    const accessToken = await AsyncStorage.getItem(UserCreds.ACCESS_TOKEN_KEY)
    const response = await clientConfig.get(
        '/favorite', {
        headers: {
            Authorization: `Bearer ${accessToken}`
        },
        params: {
            page: page
        }
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const getDetailRecipe = async (recipeId) => {
    const accessToken = await AsyncStorage.getItem(UserCreds.ACCESS_TOKEN_KEY)
    const response = await clientConfig.get(
        `/recipe/${recipeId}`, {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const postFavorite = async (recipeId) => {
    const accessToken = await AsyncStorage.getItem(UserCreds.ACCESS_TOKEN_KEY)
    const response = await clientConfig.post(
        `/favorite/${recipeId}`,
        null, {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const deleteFavorite = async (recipeId) => {
    const accessToken = await AsyncStorage.getItem(UserCreds.ACCESS_TOKEN_KEY)
    const response = await clientConfig.delete(
        `/favorite/${recipeId}`, {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}
import axios from "axios";

const clientConfig = axios.create({
    baseURL: "https://recipe.timedoor.id/api/v1"
})

clientConfig.interceptors.request.use(
    async (config) => {
        config.headers['Content-Type'] = 'application/json'
        config.headers.post['Content-Type'] = 'application/json'
        config.headers.Accept = 'application/json'

        return config
    },
    (err) => {
        return Promise.reject(err)
    }
)

export default clientConfig